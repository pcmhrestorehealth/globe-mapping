library("gdata")
library("ggplot2")
library("ggthemes")
library("dplyr")
theme_set(theme_bw())
library("sf")
library("tidyverse")

library("rnaturalearth")
library("rnaturalearthdata")

download.file("https://globeproject.com/data/GLOBE-Phase-2-Aggregated-Societal-Culture-Data.xls", "culture-dimensions.xls")

trim <- function (x) gsub("^\\s+|\\s+$", "", x)

countries = read.xls("culture-dimensions.xls", strip.white=TRUE)

countries$Country.Cluster <- trim(countries$Country.Cluster)

world <- ne_countries(scale = "medium", returnclass = "sf")

world <- world %>% filter(!name %in% c("Fr. S. Antarctic Lands", "Antarctica"))

world <- world %>% mutate(name = recode(name, 
                                        `United Kingdom` = 'England',
                                        `Czech Rep.` = 'Czech Republic',
                                        `Korea` = 'South Korea',
                                        `United States` = 'USA'
                                        )
                          )



countries <- countries %>% mutate(Country.Name = recode(Country.Name,
                                                        `IRAN` = 'Iran',
                                                        `South Africa (Black Sample)` = 'South Africa',
                                                        `Canada (English-speaking)` = 'Canada',
                                                        `Germany (EAST)` = 'Germany'
                                                        ))

countries <- countries %>% filter(Country.Name != 'French Switzerland' & Country.Name != 'South Africa (White Sample)' & Country.Name != 'Germany (WEST)' & Country.Cluster != "")

joined_data <- st_sf(left_join(countries, world, by = c("Country.Name" = "name")))

ggplot(data = world) +
  geom_sf(lwd=0.1) +
  geom_sf(data = joined_data, aes(fill = Country.Cluster), lwd=0.1) +
  labs(fill='Country Clusters') +
  theme_map() 


ggsave("map.png", width=6.54*2, height=4.06*2)
